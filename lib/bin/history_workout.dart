// // ignore_for_file: prefer_const_constructors, avoid_print, unnecessary_new

// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';

// import '../workout.dart';
// import 'workout_widget.dart';

// class HistoryWorkout extends StatefulWidget {
//   const HistoryWorkout({Key? key}) : super(key: key);

//   @override
//   _HistoryWorkoutState createState() => _HistoryWorkoutState();
// }

// class _HistoryWorkoutState extends State<HistoryWorkout> {
//   final Stream<QuerySnapshot> _workoutStream =
//       FirebaseFirestore.instance.collection('workouts').snapshots();
//   CollectionReference workouts =
//       FirebaseFirestore.instance.collection('workouts');
//   double sum = 0;
//   @override
//   void initState() {
//     super.initState();
//   }

//   Future<void> delWorkout(workoutId) {
//     return workouts
//         .doc(workoutId)
//         .delete()
//         .then((value) => print('Workout Delete'))
//         .catchError((error) => print('Failed to delete workout: $error'));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('ประวัติการออกกำลังกาย'),
//       ),
//       body: Column(
//         children: [
//           Expanded(
//             child: StreamBuilder(
//                 stream: _workoutStream,
//                 builder: (BuildContext context,
//                     AsyncSnapshot<QuerySnapshot> snapshot) {
//                   if (snapshot.hasError) {
//                     return Text('Something went wrong');
//                   }
//                   if (snapshot.connectionState == ConnectionState.waiting) {
//                     return Text('Loading...');
//                   }
//                   return ListView(
//                       children:
//                           snapshot.data!.docs.map((DocumentSnapshot document) {
//                     Map<String, dynamic> data =
//                         document.data()! as Map<String, dynamic>;

//                     sum += data['totalCal'];
//                     return Column(
//                       children: [
//                         Container(
//                           margin: EdgeInsets.all(20),
//                           child: ListTile(
//                             onTap: () async {
//                               Navigator.push(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) => WorkoutWidget(
//                                             workout: Workout(
//                                                 nameWorkout:
//                                                     data['nameWorkout'],
//                                                 set: data['set'],
//                                                 totalCal: data['totalCal']),
//                                           )));
//                             },
//                             title: Text(data['nameWorkout']),
//                             subtitle: Text(
//                                 'จำนวน: ${data['set']} แคลอรี่:${data['totalCal']}'),
//                             trailing: IconButton(
//                                 onPressed: () async {
//                                   await delWorkout(document.id);
//                                 },
//                                 icon: Icon(Icons.delete, color: Colors.red)),
//                           ),
//                         ),
//                       ],
//                     );
//                   }).toList());
//                 }),
//           ),
//           Container(
//               margin: EdgeInsets.all(20), child: Text('Total calories:$sum ')),
//         ],
//       ),
//     );
//   }
// }
