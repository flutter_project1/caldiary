// // ignore_for_file: prefer_const_constructors, no_logic_in_create_state, must_be_immutable, non_constant_identifier_names

// import 'package:caldiary/bin/workout.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:google_fonts/google_fonts.dart';

// class WorkoutWidget extends StatefulWidget {
//   Workout workout;
//   WorkoutWidget({Key? key, required this.workout}) : super(key: key);

//   @override
//   _WorkoutWidgetState createState() => _WorkoutWidgetState(workout);
// }

// class _WorkoutWidgetState extends State<WorkoutWidget> {
//   @override
//   initState() {
//     super.initState();
//     if (workout.nameWorkout != "") {
//       setState(() {
//         name_workout = workout.nameWorkout;
//         set = workout.set;
//         totalCal = workout.totalCal;
//       });
//     }
//   }

//   CollectionReference workouts =
//       FirebaseFirestore.instance.collection('workouts');
//   final _formKey = GlobalKey<FormState>();

//   Workout workout;
//   _WorkoutWidgetState(this.workout);
//   String name_workout = "-";
//   String alert_error = "";
//   List<double> workoutCal = [0, 455, 300, 400, 400, 200, 200, 200, 200];
//   double set = 0;
//   double totalCal = 0;
//   double sum = 0;

//   Future<void> addNewWorkout() {
//     return Future.delayed(Duration(seconds: 0), () {
//       workouts
//           .add({
//             "nameWorkout": workout.nameWorkout,
//             "set": workout.set,
//             "totalCal": workout.totalCal
//           })
//           .then((value) => print('Added workout'))
//           .catchError((error) => print('Failed to add workout: $error'));
//     });
//   }

//   Future<void> saveWorkout(workoutId) {
//     return workouts
//         .doc(workoutId)
//         .update({
//           "nameWorkout": workout.nameWorkout,
//           "set": workout.set,
//           "totalCal": workout.totalCal
//         })
//         .then((value) => print('Workout update'))
//         .catchError((error) => print('Failed to update workout: $error'));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Calorie Diary", style: GoogleFonts.lemon()),
//         centerTitle: true,
//         flexibleSpace: Container(
//           decoration: BoxDecoration(
//               gradient: LinearGradient(
//             colors: [Colors.blue.shade800, Colors.lightBlue.shade400],
//           )),
//         ),
//       ),
//       body: Container(
//         padding: const EdgeInsets.all(50.0),
//         child: ListView(
//           children: [
//             Text('$sum'),
//             Form(
//               key: _formKey,
//               child: Column(
//                 children: [
//                   Row(
//                     children: [
//                       const Text(
//                         'ออกกำลังกาย',
//                         style: TextStyle(fontSize: 20),
//                       ),
//                       Expanded(child: Container()),
//                       DropdownButton(
//                           items: [
//                             DropdownMenuItem(
//                               child: Text('เลือก'),
//                               value: '-',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[0];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('cardio'),
//                               value: 'cardio',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[1];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('dancing'),
//                               value: "dancing",
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[2];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('hula hooping'),
//                               value: 'hula hooping',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[3];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('swimming'),
//                               value: 'swimming',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[4];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text(
//                                 'upper body exercises',
//                               ),
//                               value: 'upper body exercises',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[5];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('lower body exercises'),
//                               value: 'lower body exercises',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[6];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('back exercises'),
//                               value: 'back exercises',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[7];
//                                 });
//                               },
//                             ),
//                             DropdownMenuItem(
//                               child: Text('core exercises'),
//                               value: 'core exercises',
//                               onTap: () {
//                                 setState(() {
//                                   totalCal = workoutCal[8];
//                                 });
//                               },
//                             ),
//                           ],
//                           value: name_workout,
//                           onChanged: (String? newValue) {
//                             setState(() {
//                               name_workout = newValue!;
//                               workout.nameWorkout = name_workout;
//                             });
//                           }),
//                     ],
//                   ),
//                   Text(
//                     alert_error,
//                     style: TextStyle(color: Colors.red),
//                   ),
//                   SizedBox(height: 40),
//                   TextFormField(
//                     initialValue: workout.set.toString(),
//                     validator: (value) {
//                       var num = double.tryParse(value!);
//                       if (num == null || double.parse(value) <= 0) {
//                         return 'กรุณาใส่จำนวน ที่มีค่ามากกว่าหรือเท่ากับ 1 ';
//                       }
//                       return null;
//                     },
//                     decoration: InputDecoration(
//                         labelText: "จำนวน(เซต/ชั่วโมง)",
//                         border: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(20)),
//                         icon: Icon(
//                           Icons.more_time,
//                           size: 40,
//                         )),
//                     keyboardType: TextInputType.number,
//                     autovalidateMode: AutovalidateMode.always,
//                     onChanged: (value) {
//                       setState(() {
//                         set = double.parse(value);
//                         workout.set = double.parse(value);
//                       });
//                     },
//                   ),
//                   SizedBox(height: 40),
//                   Row(mainAxisAlignment: MainAxisAlignment.center, children: [
//                     IconTheme(
//                         data: IconThemeData(color: Colors.red),
//                         child: Icon(
//                           Icons.local_fire_department,
//                           size: 40,
//                         )),
//                     Text("${totalCal * set} แคลอรี่ ",
//                         style: TextStyle(fontSize: 30)),
//                   ]),
//                   SizedBox(
//                     height: 40,
//                   ),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Container(
//                         margin: EdgeInsets.all(10),
//                         child: ElevatedButton(
//                             onPressed: () async {
//                               setState(() {
//                                 workout.totalCal = set * totalCal;
//                               });
//                               if (_formKey.currentState!.validate() &&
//                                   name_workout != '-') {
//                                 if (workout.nameWorkout != "") {
//                                   Navigator.pop(context);
//                                 }
//                                 await addNewWorkout();
//                                 Navigator.pop(context);
//                               } else {
//                                 setState(() {
//                                   alert_error = "**กรุณาเลือกการออกกำลังกาย**";
//                                 });
//                               }
//                               // await addWorkout();
//                             },
//                             child: Text("บันทึก"),
//                             style: ElevatedButton.styleFrom(
//                                 textStyle: TextStyle(fontSize: 25))),
//                       ),
//                       Container(
//                         margin: EdgeInsets.all(20),
//                         child: ElevatedButton(
//                             onPressed: () async {
//                               setState(() {
//                                 workout.totalCal = set * totalCal;
//                               });
//                               if (_formKey.currentState!.validate() &&
//                                   name_workout != '-') {
//                                 setState(() {
//                                   sum += totalCal;
//                                   name_workout = "-";
//                                   set = 0;
//                                   totalCal = 0;
//                                 });
//                                 await addNewWorkout();
//                               } else {
//                                 setState(() {
//                                   alert_error = "**กรุณาเลือกการออกกำลังกาย**";
//                                 });
//                               }
//                               // await addWorkout();
//                             },
//                             child: Text("บันทึกและค้นหาต่อ"),
//                             style: ElevatedButton.styleFrom(
//                                 textStyle: TextStyle(fontSize: 25))),
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
