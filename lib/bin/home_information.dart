import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HomeInformation extends StatefulWidget {
  const HomeInformation({Key? key}) : super(key: key);

  @override
  _HomeInformationState createState() => _HomeInformationState();
}

class _HomeInformationState extends State<HomeInformation> {
  Stream<QuerySnapshot> _userStream =
      FirebaseFirestore.instance.collection('users').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('แสดงข้อมูลผู้ใช้')),
      body: StreamBuilder(
        stream: _userStream,
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: const CircularProgressIndicator(),
            );
          }
          return ListView(
            children: snapshot.data!.docs.map((document) {
              return Container(
                child: ListTile(
                    leading: CircleAvatar(
                  radius: 30,
                  child: FittedBox(
                    child: Text('${document['age']}'),
                  ),
                )),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
