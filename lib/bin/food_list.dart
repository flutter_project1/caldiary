// ignore_for_file: non_constant_identifier_names

class FoodList {
  final List<Map<String, dynamic>> _foodList = [
    {"name": "น้ำแดง", "calories": 180},
    {"name": "น้ำเขียว", "calories": 180},
    {"name": "น้ำส้ม", "calories": 100},
    {"name": "น้ำองุ่น", "calories": 130},
    {"name": "ไข่ดาว", "calories": 112},
    {"name": "ข้าวกระเพราหมูสับ", "calories": 500},
    {"name": "สุกี้น้ำไก่", "calories": 345},
    {"name": "ก๋วยเตี๋ยวน้ำใส", "calories": 400},
    {"name": "ก๋วยเตี๋ยวต้มยำ", "calories": 500},
    {"name": "เกาเหลา", "calories": 300},
  ];
}
