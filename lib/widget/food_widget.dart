// ignore_for_file: prefer_const_constructors, avoid_print

import 'package:caldiary/widget/food_form.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FoodWidget extends StatefulWidget {
  const FoodWidget({Key? key}) : super(key: key);

  @override
  _FoodWidgetState createState() => _FoodWidgetState();
}

class _FoodWidgetState extends State<FoodWidget> {
  final List<Map<String, dynamic>> _foodList = [
    {"name": "น้ำแดง", "calories": 180},
    {"name": "น้ำเขียว", "calories": 180},
    {"name": "น้ำส้ม", "calories": 100},
    {"name": "น้ำองุ่น", "calories": 130},
    {"name": "ไข่ดาว", "calories": 112},
    {"name": "ข้าวกระเพราหมูสับ", "calories": 500},
    {"name": "สุกี้น้ำไก่", "calories": 345},
    {"name": "ก๋วยเตี๋ยวน้ำใส", "calories": 400},
    {"name": "ก๋วยเตี๋ยวต้มยำ", "calories": 500},
    {"name": "เกาเหลา", "calories": 300},
  ];

  List<Map<String, dynamic>> _searchFood = [];
  @override
  initState() {
    _searchFood = _foodList;
    super.initState();
  }

  void _runFilter(String enteredKeyword) {
    List<Map<String, dynamic>> results = [];
    if (enteredKeyword.isEmpty) {
      results = _foodList;
    } else {
      results = _foodList
          .where((user) =>
              user["name"].toLowerCase().contains(enteredKeyword.toLowerCase()))
          .toList();
    }
    setState(() {
      _searchFood = results;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("รายการอาหาร", style: GoogleFonts.lemon()),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
          )),
        ),
      ),
      body: Container(
        margin:
            const EdgeInsets.only(top: 20.0, left: 30, right: 30, bottom: 30),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            TextField(
              onChanged: (value) => _runFilter(value),
              decoration: const InputDecoration(
                  labelText: 'ค้นหา', suffixIcon: Icon(Icons.search)),
            ),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: _searchFood.isNotEmpty
                  ? ListView.builder(
                      itemCount: _searchFood.length,
                      itemBuilder: (context, index) => Card(
                        key: ValueKey(_searchFood[index]["id"]),
                        color: Colors.amberAccent,
                        elevation: 4,
                        margin: const EdgeInsets.symmetric(vertical: 5),
                        child: ListTile(
                          title: Text(_searchFood[index]['name']),
                          subtitle: Text(
                              'calrories ${_searchFood[index]["calories"].toString()}'),
                          onTap: () {
                            print(_searchFood[index]['name'] +
                                " " +
                                _searchFood[index]["calories"].toString());
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FoodForm(
                                    nameFood: _searchFood[index]["name"],
                                    totalCal: _searchFood[index]['calories'],
                                    sum_food: _searchFood[index]['calories'],
                                    qty: 1,
                                    foodId: '',
                                  ),
                                ));
                          },
                        ),
                      ),
                    )
                  : const Text(
                      'No results found',
                      style: TextStyle(fontSize: 24),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
