// ignore_for_file: prefer_const_constructors, no_logic_in_create_state, must_be_immutable, unnecessary_this, non_constant_identifier_names, unnecessary_new, prefer_final_fields, unused_field

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WorkoutForm extends StatefulWidget {
  String workoutId;
  WorkoutForm({Key? key, required this.workoutId}) : super(key: key);

  @override
  _WorkoutFormState createState() => _WorkoutFormState(this.workoutId);
}

class _WorkoutFormState extends State<WorkoutForm> {
  String workoutId;
  _WorkoutFormState(this.workoutId);

  final _formKey = GlobalKey<FormState>();
  final String? _email = FirebaseAuth.instance.currentUser!.email;

  TextEditingController _setWorkoutController = new TextEditingController();
  CollectionReference workouts =
      FirebaseFirestore.instance.collection('workouts');
  TextEditingController _totalCalWorkoutController =
      new TextEditingController();

  List<double> workoutCal = [0, 455, 300, 400, 400, 200, 200, 200, 200];
  String name_workout = "-";
  double set = 1;
  double totalCal = 0, sum = 0;
  String alert_error = "";

  @override
  initState() {
    super.initState();
    if (this.workoutId.isNotEmpty) {
      workouts.doc(this.workoutId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            name_workout = data['nameWorkout'];
            set = data['set'];
            totalCal = data['totalCal'];
            sum = totalCal / set;
            totalCal = totalCal / set;
          });
          _setWorkoutController.text = set.toString();
        }
      });
    }
  }

  Future<void> addWorkout() {
    return workouts
        .add({
          'time': Timestamp.now(),
          'nameWorkout': name_workout,
          'set': set,
          'totalCal': totalCal,
          'email': _email
        })
        .then((value) => print("add new workout"))
        .catchError((error) => print('Failed to add workout: $error'));
  }

  Future<void> updateWorkout() {
    return workouts
        .doc(workoutId)
        .update({
          'time': Timestamp.now(),
          'nameWorkout': name_workout,
          'set': set,
          'totalCal': totalCal,
          'email': _email
        })
        .then((value) => print('update workout'))
        .catchError((error) => print('Failed to update workout: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ออกกำลังกาย", style: GoogleFonts.lemon()),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
          )),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(50.0),
        child: ListView(
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text(
                        'ออกกำลังกาย',
                        style: TextStyle(fontSize: 20),
                      ),
                      Expanded(child: Container()),
                      DropdownButton(
                          items: [
                            DropdownMenuItem(
                              child: Text('เลือก'),
                              value: '-',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[0];
                                  sum = workoutCal[0];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('cardio'),
                              value: 'cardio',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[1];
                                  sum = workoutCal[1];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('dancing'),
                              value: "dancing",
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[2];
                                  sum = workoutCal[2];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('hula hooping'),
                              value: 'hula hooping',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[3];
                                  sum = workoutCal[3];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('swimming'),
                              value: 'swimming',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[4];
                                  sum = workoutCal[4];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text(
                                'upper body exercises',
                              ),
                              value: 'upper body exercises',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[5];
                                  sum = workoutCal[5];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('lower body exercises'),
                              value: 'lower body exercises',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[6];
                                  sum = workoutCal[6];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('back exercises'),
                              value: 'back exercises',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[7];
                                  sum = workoutCal[7];
                                });
                              },
                            ),
                            DropdownMenuItem(
                              child: Text('core exercises'),
                              value: 'core exercises',
                              onTap: () {
                                setState(() {
                                  totalCal = workoutCal[8];
                                  sum = workoutCal[8];
                                });
                              },
                            ),
                          ],
                          value: name_workout,
                          onChanged: (String? newValue) {
                            setState(() {
                              name_workout = newValue!;
                              // set = 0;
                            });
                          }),
                    ],
                  ),
                  Text(
                    alert_error,
                    style: TextStyle(color: Colors.red),
                  ),
                  SizedBox(height: 40),
                  TextFormField(
                    // initialValue: set.toString(),
                    controller: _setWorkoutController,
                    validator: (value) {
                      var num = double.tryParse(value!);
                      if (num == null || double.parse(value) <= 0) {
                        return 'กรุณาใส่จำนวน ที่มีค่ามากกว่าหรือเท่ากับ 1 ';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        labelText: "จำนวน(เซต/ชั่วโมง)",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)),
                        icon: Icon(
                          Icons.more_time,
                          size: 40,
                        )),
                    keyboardType: TextInputType.number,
                    autovalidateMode: AutovalidateMode.always,
                    onChanged: (value) {
                      setState(() {
                        set = double.parse(value);
                      });
                    },
                  ),
                  SizedBox(height: 40),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    IconTheme(
                        data: IconThemeData(color: Colors.red),
                        child: Icon(
                          Icons.local_fire_department,
                          size: 40,
                        )),
                    Text("${sum * set} แคลอรี่ ",
                        style: TextStyle(fontSize: 30)),
                  ]),
                  SizedBox(
                    height: 40,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          totalCal = totalCal * set;
                        });
                        if (_formKey.currentState!.validate() &&
                            name_workout != '-') {
                          if (workoutId.isEmpty) {
                            await addWorkout();
                          } else {
                            await updateWorkout();
                          }
                          Navigator.pop(context);
                        } else if (name_workout == "-") {
                          setState(() {
                            alert_error = "**กรุณาเลือกการออกกำลังกาย**";
                          });
                        }
                      },
                      child: Text("บันทึก"),
                      style: ElevatedButton.styleFrom(
                          textStyle: TextStyle(fontSize: 25))),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
