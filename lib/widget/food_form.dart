// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, no_logic_in_create_state, must_be_immutable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FoodForm extends StatefulWidget {
  String foodId, nameFood;
  double qty, totalCal, sum_food;
  FoodForm(
      {Key? key,
      required this.foodId,
      required this.nameFood,
      required this.totalCal,
      required this.qty,
      required this.sum_food})
      : super(key: key);

  @override
  _HistoryFoodState createState() =>
      _HistoryFoodState(foodId, nameFood, qty, totalCal, sum_food);
}

class _HistoryFoodState extends State<FoodForm> {
  _HistoryFoodState(
      this.foodId, this.nameFood, this.qty, this.totalCal, this.sum_food);
  String foodId, nameFood;
  double qty, totalCal, sum_food;
  final _formKey = GlobalKey<FormState>();
  final String? _email = FirebaseAuth.instance.currentUser!.email;
  CollectionReference food_list =
      FirebaseFirestore.instance.collection('food_list');

  @override
  void initState() {
    super.initState();
    if (this.foodId.isNotEmpty) {
      food_list.doc(this.foodId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          setState(() {
            nameFood = data['nameFood'];
            qty = data['qty'];
            totalCal = data['totalCal'];
            sum_food = sum_food;
            sum_food = sum_food / qty;
            totalCal = totalCal / qty;
          });
          // _setWorkoutController.text = set.toString();
        }
      });
    }
  }

  Future<void> addFood() {
    return food_list
        .add({
          'time': Timestamp.now(),
          'nameFood': nameFood,
          'qty': qty,
          'totalCal': totalCal,
          'email': _email
        })
        .then((value) => print("add new food"))
        .catchError((error) => print('Failed to add food: $error'));
  }

  Future<void> updateFood() {
    return food_list
        .doc(foodId)
        .update({
          'time': Timestamp.now(),
          'nameFood': nameFood,
          'qty': qty,
          'totalCal': totalCal,
          'email': _email
        })
        .then((value) => print('update food'))
        .catchError((error) => print('Failed to update food: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("อาหาร", style: GoogleFonts.lemon()),
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
            )),
          ),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 40.0),
          child: ListView(
            children: [
              Container(
                child: Form(
                  key: _formKey,
                  child: Center(
                    child: Text(
                      nameFood,
                      style: TextStyle(fontSize: 50),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(40.0),
                child: TextFormField(
                  initialValue: qty.toString(),
                  validator: (value) {
                    var num = double.tryParse(value!);
                    if (num == null || double.parse(value) <= 0) {
                      return 'กรุณาใส่จำนวน ที่มีค่ามากกว่าหรือเท่ากับ 1 ';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    icon: Icon(Icons.format_list_numbered),
                    // labelText: "",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    // icon: Icon(
                    //   Icons.restart_alt,
                    //   size: 40,
                    // )
                  ),
                  keyboardType: TextInputType.number,
                  autovalidateMode: AutovalidateMode.always,
                  onChanged: (value) {
                    setState(() {
                      qty = double.parse(value);
                    });
                  },
                ),
              ),
              Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                    IconTheme(
                        data: IconThemeData(color: Colors.red),
                        child: Icon(
                          Icons.restaurant_menu,
                          size: 40,
                        )),
                    Text("${sum_food * qty} แคลอรี่ ",
                        style: TextStyle(fontSize: 30)),
                  ])),
              Container(height: 20),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        setState(() {
                          print('$sum_food $qty');
                          totalCal = sum_food * qty;
                          print(totalCal);
                        });
                        if (foodId.isEmpty) {
                          await addFood();
                        } else {
                          await updateFood();
                        }
                      }
                      Navigator.pop(context);
                    },
                    child: Text("บันทึก"),
                    style: ElevatedButton.styleFrom(
                        textStyle: TextStyle(fontSize: 30))),
              ])
            ],
          ),
        ));
  }
}
