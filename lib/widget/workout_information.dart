// ignore_for_file: camel_case_types

import 'package:caldiary/widget/home_widget.dart';
import 'package:caldiary/widget/workout_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WorkoutInformation extends StatefulWidget {
  const WorkoutInformation({Key? key}) : super(key: key);

  @override
  _workoutInformationState createState() => _workoutInformationState();
}

class _workoutInformationState extends State<WorkoutInformation> {
  late final Stream<QuerySnapshot> _workoutStream;
  CollectionReference workouts =
      FirebaseFirestore.instance.collection('workouts');

  String? _email = "";
  @override
  void initState() {
    super.initState();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email;
      _workoutStream = FirebaseFirestore.instance
          .collection('workouts')
          .where('email', isEqualTo: _email)
          // .orderBy('time', descending: true)
          .snapshots();
    });
  }

  Future<void> delWorkout(workoutId) {
    return workouts
        .doc(workoutId)
        .delete()
        .then((value) => print('Workout delete'))
        .catchError((error) => print('Failed to delete workout: $error'));
  }

  double sum = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        //         Future<void> addWorkout() {
        //   return workouts
        //       .add({
        //         'time': Timestamp.now(),
        //         'nameWorkout': name_workout,
        //         'set': set,
        //         'totalCal': totalCal,
        //         'email': _email
        //       })
        //       .then((value) => print("add new workout"))
        //       .catchError((error) => print('Failed to add workout: $error'));
        // }
        print('Total: $sum');
        Navigator.pop(
            context, MaterialPageRoute(builder: (context) => HomeWidget()));
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("ออกกำลังกาย", style: GoogleFonts.lemon()),
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
            )),
          ),
        ),
        body: Column(
          children: [
            // Text('Total: $sum'),
            Expanded(
              child: StreamBuilder(
                  stream: _workoutStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something went wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Text('Loading...');
                    }
                    return Container(
                      margin: const EdgeInsets.all(15),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                              document.data()! as Map<String, dynamic>;
                          sum += data['totalCal'];
                          // print('$sum');
                          return Container(
                            margin: const EdgeInsets.only(top: 2, bottom: 2),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(.2),
                                  blurRadius: 20.0,
                                ),
                              ],
                            ),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              color: const Color(0xFFf4f4f4),
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              child: ListTile(
                                title: Text(data['nameWorkout']),
                                subtitle: Text(
                                    'จำนวน: ${data['set']}  แคลอรี่: ${data['totalCal']}'),
                                trailing: IconButton(
                                    onPressed: () async {
                                      await delWorkout(document.id);
                                    },
                                    icon: const Icon(Icons.delete)),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => WorkoutForm(
                                              workoutId: document.id)));
                                },
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    );
                  }),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WorkoutForm(workoutId: '')));
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
