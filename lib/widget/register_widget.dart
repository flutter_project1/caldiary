// ignore_for_file: prefer_const_constructors, unnecessary_null_comparison

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class RegisterWidget extends StatefulWidget {
  const RegisterWidget({Key? key}) : super(key: key);

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String sex = "-";
  int age = 0;

  Future<void> addNewUser() {
    return users
        .add({
          'email': email,
          'sex': sex,
          "age": age,
          "height": 0,
          "weight": 0,
          "bmi": 0,
          "bmrF": 0,
          "bmrM": 0,
          "goalWater": 0
        })
        .then((value) => print("add new user"))
        .catchError((error) => print('Failed to add user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ลงทะเบียน", style: GoogleFonts.lemon()),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.green.shade800, Colors.lightGreen.shade400],
          )),
        ),
      ),
      body: Form(
          key: _formKey,
          child: Container(
            margin: const EdgeInsets.all(16.0),
            padding: const EdgeInsets.all(16.0),
            child: ListView(children: [
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'กรุณากรอก Gmail ที่ต้องการลงทะเบียน';
                  }
                  return null;
                },
                decoration: const InputDecoration(labelText: "Gmail"),
                onChanged: (value) {
                  setState(() {
                    email = value;
                  });
                },
              ),
              SizedBox(
                height: 20,
              ),
              DropdownButtonFormField(
                value: sex,
                decoration: InputDecoration(labelText: "เพศ"),
                validator: (value) {
                  if (value == "-") {
                    return 'กรุณาใส่เพศ';
                  }
                  return null;
                },
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Text('เลือก'),
                      ],
                    ),
                    value: "-",
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        Text('ชาย'),
                      ],
                    ),
                    value: 'ชาย',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        Text('หญิง'),
                      ],
                    ),
                    value: 'หญิง',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    sex = newValue!;
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || int.parse(value) < 19) {
                    return 'กรุณาใส่อายุ มากกว่าหรือเท่ากับ 20 ปี ';
                  }
                  return null;
                },
                decoration: const InputDecoration(labelText: "อายุ"),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                onChanged: (value) {
                  setState(() {
                    age = int.parse(value);
                  });
                },
              ),
              // const SizedBox(
              //   height: 20,
              // ),
              // TextFormField(
              //   validator: (value) {
              //     late var num = double.tryParse(value!);
              //     if (num == null || num == 0) {
              //       return 'กรุณาใส่ส่วนสูงเป็นเซนติเมตร สามารถใส่จุดทศนิยมได้';
              //     }
              //     return null;
              //   },
              //   decoration: const InputDecoration(labelText: "ส่วนสูง(ซ.ม.)"),
              //   keyboardType: TextInputType.number,
              //   onChanged: (value) {
              //     setState(() {
              //       height = double.parse(value);
              //     });
              //   },
              // ),
              // const SizedBox(
              //   height: 20,
              // ),
              // TextFormField(
              //   validator: (value) {
              //     late var num = double.tryParse(value!);
              //     if (num == null || num == 0) {
              //       return 'กรุณาใส่น้ำหนักเป็นกิโลกรัม สามารถใส่จุดทศนิยมได้';
              //     }
              //     return null;
              //   },
              //   decoration: const InputDecoration(labelText: "น้ำหนัก(ก.ก.)"),
              //   keyboardType: TextInputType.number,
              //   onChanged: (value) {
              //     setState(() {
              //       weight = double.parse(value);
              //     });
              //   },
              // ),
              const SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                      child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.green.shade400),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        print('pass');
                        await addNewUser();
                        Navigator.pop(context);
                      } else {
                        print('fail');
                      }
                    },
                    child: Text('ลงทะเบียน'),
                  ))
                ],
              ),
            ]),
          )),
    );
  }
}
