// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, unnecessary_string_interpolations, prefer_final_fields, unrelated_type_equality_checks, prefer_collection_literals, deprecated_member_use, no_logic_in_create_state, must_be_immutable

import 'package:caldiary/widget/food_information.dart';
import 'package:caldiary/widget/profile_widget.dart';
import 'package:caldiary/widget/workout_information.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'food_widget.dart';

class HomeWidget extends StatefulWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  // home
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  late Stream<QuerySnapshot> _userStream;
  String? _email = "";
  String? _name = "";
  String? _imageUrl = "";

  double kcal = 0;
  double drinkWater = 0;

  // food
  double foodCal = 0;

  // workout
  double workOutCal = 0;

  @override
  void initState() {
    super.initState();
    _loadDrink();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email;
      _name = FirebaseAuth.instance.currentUser!.displayName;
      _imageUrl = FirebaseAuth.instance.currentUser!.photoURL;
      _userStream = FirebaseFirestore.instance
          .collection('users')
          .where('email', isEqualTo: _email)
          .snapshots();
    });
  }

  Future<void> _loadDrink() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      drinkWater = prefs.getDouble('drink_water') ?? 0;
    });
  }

  Future<void> _saveDrink() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setDouble('drink_water', drinkWater);
    });
  }

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _userStream,
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('Error'),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Container(
            margin: EdgeInsets.all(20.0),
            child: ListView(
                shrinkWrap: true,
                children: snapshot.data!.docs.map((document) {
                  Future<void> _resetProfile() async {
                    final prefs = await SharedPreferences.getInstance();
                    prefs.remove('drink_water');
                    await _loadDrink();
                    return users.doc(document.id).update({
                      'email': _email,
                      'sex': "-",
                      "age": 0,
                      "height": 0,
                      "weight": 0,
                      "bmi": 0,
                      "bmrF": 0,
                      "bmrM": 0,
                      "goalWater": 0
                    });
                  }

                  return Column(
                    children: [
                      Card(
                        clipBehavior: Clip.antiAlias,
                        color: Color(0xFFf4f4f4),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20))),
                        child: Column(
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                  child: ListTile(
                                    leading: GestureDetector(
                                      child: Container(
                                        width: 50,
                                        height: 80,
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 1.0),
                                        alignment: Alignment.center,
                                        child: CircleAvatar(
                                          child: (_imageUrl == null)
                                              ? Icon(Icons.person)
                                              : Image.network(_imageUrl!),
                                        ),
                                      ),
                                    ),
                                    // Icon(Icons.person),

                                    title: Text('$_name'),
                                    subtitle: Text(
                                      'เพศ: ${document['sex']}  อายุ: ${document['age']} ปี \nส่วนสูง: ${document['height']} ซ.ม. น้ำหนัก: ${document['weight']} ก.ก.',
                                      style: TextStyle(
                                          color: Colors.black.withOpacity(0.6)),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 20),
                                  child: IconButton(
                                    onPressed: () async {
                                      _signOut();
                                    },
                                    icon: Icon(Icons.logout),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.all(20.0),
                                          child: Column(
                                            children: [
                                              Text(
                                                'BMI',
                                                style: TextStyle(
                                                    fontSize: 30.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Container(
                                                  child: (document['bmi'].isNaN)
                                                      ? Text(
                                                          '-',
                                                          style: TextStyle(
                                                              fontSize: 20.0),
                                                        )
                                                      : Text(
                                                          '${document['bmi'].toStringAsFixed(2)}',
                                                          style: TextStyle(
                                                              fontSize: 20.0,
                                                              fontWeight: (!document[
                                                                          'bmi']
                                                                      .isNaN)
                                                                  ? FontWeight
                                                                      .bold
                                                                  : FontWeight
                                                                      .normal,
                                                              color: (document[
                                                                          'bmi']
                                                                      .isNaN)
                                                                  ? Color(
                                                                      0xFF000000)
                                                                  : (document['bmi'] >=
                                                                              30 ||
                                                                          document['bmi'] <=
                                                                              18.5)
                                                                      ? Color(
                                                                          0xFFcc0000)
                                                                      : (document['bmi'] >= 25 &&
                                                                              document['bmi'] <=
                                                                                  29.9)
                                                                          ? Color(
                                                                              0xFFedaf1f)
                                                                          : Color(
                                                                              0xFF298c29))))
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(20.0),
                                          child: Column(
                                            children: [
                                              Text('BMR',
                                                  style: TextStyle(
                                                      fontSize: 30.0,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Container(
                                                  child: (document['sex'] ==
                                                          '-')
                                                      ? Text(
                                                          '-',
                                                          style: TextStyle(
                                                              fontSize: 20.0),
                                                        )
                                                      : (document['sex'] ==
                                                              'หญิง')
                                                          ? Text(
                                                              '${document['bmrF'].toStringAsFixed(0)}',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      21.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            )
                                                          : Text(
                                                              '${document['bmrM'].toStringAsFixed(0)}',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      21.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ))
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Center(
                                          child: Text(
                                              'เป้าหมายการดื่มน้ำ (${document['goalWater'].toStringAsFixed(0)} ml.)',
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold)),
                                        ),
                                        Center(
                                            child: ((drinkWater /
                                                        (document['goalWater'] /
                                                            100))
                                                    .isNaN
                                                ? Text('-',
                                                    style: TextStyle(
                                                      fontSize: 20.0,
                                                    ))
                                                : ((drinkWater /
                                                                (document['goalWater'] /
                                                                    100))
                                                            .ceilToDouble() >=
                                                        100)
                                                    ? Text(
                                                        '${(drinkWater / (document['goalWater'] / 100)).toStringAsFixed(0)} %',
                                                        style: TextStyle(
                                                            fontSize: 20.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Color(
                                                                0xFF2aa4eb)))
                                                    : Text(
                                                        '${(drinkWater / (document['goalWater'] / 100)).toStringAsFixed(0)} %',
                                                        style: TextStyle(
                                                            fontSize: 20.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Color(
                                                                0xFFcc0000)))))
                                      ],
                                    )
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(
                                      'วันนี้',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      '(เหลือแคลอรี่ทานได้อีก)',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w300),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.all(15.0),
                                      padding: const EdgeInsets.all(3.0),
                                      decoration: BoxDecoration(
                                          // color: Color(0xFFb9e0a5),
                                          border: Border.all(
                                              color: Color(0xFFf5f5f5),
                                              width: 5),
                                          borderRadius:
                                              BorderRadius.circular(20.0)),
                                      child: Center(
                                          child: (document['sex'] == '-')
                                              ? Text(
                                                  '-',
                                                  style:
                                                      TextStyle(fontSize: 35.0),
                                                )
                                              : (document['sex'] == 'หญิง')
                                                  ? Text(
                                                      '${(document['bmrF'] + workOutCal - foodCal).toStringAsFixed(0)} ',
                                                      style: TextStyle(
                                                          fontSize: 35.0),
                                                    )
                                                  : Text(
                                                      '${(document['bmrM'] + workOutCal - foodCal).toStringAsFixed(0)}',
                                                      style: TextStyle(
                                                          fontSize: 35.0),
                                                    )),
                                    )
                                  ],
                                )
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.end,
                              children: [
                                TextButton(
                                    onPressed: () async {
                                      await _resetProfile();
                                    },
                                    child: Text('ล้างข้อมูล')),
                              ],
                            )
                          ],
                        ),
                      ),
                      Container(margin: EdgeInsets.all(10)),
                      // นับปริมาณน้ำที่ต้องดื่มต่อวัน
                      Container(
                        color: Color(0xFFc3e6fa).withOpacity(0.25),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.local_drink_rounded,
                                        color: Color(0xFF2aa4eb),
                                      ),
                                      Text(
                                          'เป้าหมายการดื่มน้ำ (${document['goalWater'].toStringAsFixed(0)} ml.)',
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 20.0, right: 20.0),
                              child: Column(
                                children: [
                                  Slider(
                                    value: drinkWater,
                                    onChanged: (newValue) {
                                      setState(() {
                                        drinkWater = newValue;
                                        _saveDrink();
                                      });
                                    },
                                    min: 0,
                                    max:
                                        document['goalWater'].round() as double,
                                    // max: goalWater.ceilToDouble(),
                                    divisions: 20,
                                    label: drinkWater.toStringAsFixed(0),
                                  ),
                                  Row(
                                    children: [
                                      Text('0',
                                          style: TextStyle(
                                            fontSize: 14,
                                          )),
                                      Spacer(),
                                      Text(
                                          document['goalWater']
                                              .toStringAsFixed(0),
                                          style: TextStyle(
                                            fontSize: 14,
                                          ))
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              ListTile(
                                leading: Icon(Icons.person),
                                title: Text('โปรไฟล์'),
                                onTap: () async {
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ProfileWidget(
                                                userId: document.id,
                                                sex: document['sex'],
                                              )));
                                  // await _loadProfile();
                                },
                              ),
                              ListTile(
                                leading: Icon(Icons.restaurant),
                                title: Text('อาหาร'),
                                trailing: IconButton(
                                  icon: Icon(Icons.history),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FoodInformation()));
                                  },
                                ),
                                onTap: () async {
                                  await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FoodWidget()));
                                },
                              ),
                              ListTile(
                                leading: Icon(Icons.fitness_center),
                                title: Text('ออกกำลังกาย'),
                                onTap: () async {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              WorkoutInformation()));
                                },
                              ),
                              Container(margin: EdgeInsets.all(20)),
                            ],
                          ))
                    ],
                  );
                }).toList()),
          );
        });
  }
}
