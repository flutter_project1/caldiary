// ignore_for_file: avoid_unnecessary_containers, prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_string_escapes, avoid_print, must_be_immutable, no_logic_in_create_state

import 'package:caldiary/widget/home_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileWidget extends StatefulWidget {
  String userId, sex;

  ProfileWidget({Key? key, required this.userId, required this.sex})
      : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState(userId, sex);
}

class _ProfileWidgetState extends State<ProfileWidget> {
  _ProfileWidgetState(this.userId, this.sex);
  String userId, sex;

  final _formKey = GlobalKey<FormState>();
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  final String? _name = FirebaseAuth.instance.currentUser!.displayName;
  final String? _email = FirebaseAuth.instance.currentUser!.email;
  final TextEditingController _ageController = TextEditingController();
  final TextEditingController _heightController = TextEditingController();
  final TextEditingController _weightController = TextEditingController();

  // String sex = '-';
  int age = 0;
  late double height = 0;
  late double weight = 0;
  late double bmi = 0;
  late double bmrM = 0;
  late double bmrF = 0;
  late double goalWater = 0;
  // late double drinkWater = 0;

  @override
  void initState() {
    super.initState();
    setState(() {
      if (userId.isNotEmpty) {
        users.doc(userId).get().then((snapshot) {
          var data = snapshot.data() as Map<String, dynamic>;
          // print(data['height']);
          sex = data['sex'];
          age = data['age'];
          height = data['height'];
          weight = data['weight'];
          bmi = data['bmi'];
          bmrM = data['bmrM'];
          bmrF = data['bmrF'];
          goalWater = data['goalWater'];
          // print('$sex');
          _ageController.text = age.toString();
          _heightController.text = height.toString();
          _weightController.text = weight.toString();
          // print(
          //     "initState profile: $sex $age $height $weight $bmi $bmrF $bmrM $goalWater");
        });
      }
    });
  }

  Future<void> updateUser() {
    return users
        .doc(userId)
        .update({
          'email': _email,
          'sex': sex,
          'age': age,
          'height': height,
          'weight': weight,
          'bmi': bmi,
          'bmrF': bmrF,
          'bmrM': bmrM,
          'goalWater': goalWater,
        })
        .then((value) => print('update user'))
        .catchError((error) => print('Failed to update user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("โปรไฟล์"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
          )),
        ),
      ),
      body: Form(
          key: _formKey,
          child: Container(
            margin: const EdgeInsets.all(16.0),
            padding: const EdgeInsets.all(16.0),
            child: ListView(children: [
              Text('$_name',
                  style: GoogleFonts.oxygen(
                      fontSize: 20, fontWeight: FontWeight.bold)),
              Container(
                height: 20,
              ),
              DropdownButtonFormField(
                value: sex,
                decoration: InputDecoration(labelText: "เพศ"),
                validator: (value) {
                  if (value == "-") {
                    return 'กรุณาใส่เพศ';
                  }
                  return null;
                },
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Text('เลือก'),
                      ],
                    ),
                    value: "-",
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        Text('ชาย'),
                      ],
                    ),
                    value: 'ชาย',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        Text('หญิง'),
                      ],
                    ),
                    value: 'หญิง',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    print(newValue);
                    sex = newValue!;
                  });
                },
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                // initialValue: age.toString(),
                controller: _ageController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if (num == null || int.parse(value) < 19) {
                    return 'กรุณาใส่อายุ มากกว่าหรือเท่ากับ 20 ปี ';
                  }
                  return null;
                },
                decoration: const InputDecoration(labelText: "อายุ"),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                onChanged: (value) {
                  setState(() {
                    age = int.parse(value);
                  });
                },
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                // initialValue: height.toString(),
                controller: _heightController,
                validator: (value) {
                  late var num = double.tryParse(value!);
                  if (num == null || num == 0) {
                    return 'กรุณาใส่ส่วนสูงเป็นเซนติเมตร สามารถใส่จุดทศนิยมได้';
                  }
                  return null;
                },
                decoration: const InputDecoration(labelText: "ส่วนสูง(ซ.ม.)"),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    height = double.parse(value);
                  });
                },
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                // initialValue: weight.toString(),
                controller: _weightController,
                validator: (value) {
                  late var num = double.tryParse(value!);
                  if (num == null || num == 0) {
                    return 'กรุณาใส่น้ำหนักเป็นกิโลกรัม สามารถใส่จุดทศนิยมได้';
                  }
                  return null;
                },
                decoration: const InputDecoration(labelText: "น้ำหนัก(ก.ก.)"),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    weight = double.parse(value);
                  });
                },
              ),
              Container(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            bmi = ((weight) / (height / 100 * height / 100));
                            bmrM = (66 +
                                    (13.7 * weight) +
                                    (5 * height) -
                                    (6.8 * age)) *
                                1.375;
                            bmrF = (665 +
                                    (9.6 * weight) +
                                    (1.8 * height) -
                                    (4.7 * age)) *
                                1.375;
                            goalWater = weight * 2.2 * 30 / 2;
                          });
                          await updateUser();
                          Navigator.pop(context);
                        }
                      },
                      child: Text(
                        'บันทึก',
                        style: TextStyle(fontSize: 30),
                      )),
                ],
              ),
            ]),
          )),
    );
  }
}
