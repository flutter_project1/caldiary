import 'package:caldiary/widget/home_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'food_form.dart';

class FoodInformation extends StatefulWidget {
  const FoodInformation({Key? key}) : super(key: key);

  @override
  _FoodInformationState createState() => _FoodInformationState();
}

class _FoodInformationState extends State<FoodInformation> {
  late final Stream<QuerySnapshot> _foodStream;
  CollectionReference food_list =
      FirebaseFirestore.instance.collection('food_list');
  double sum = 0;

  String? _email = "";
  @override
  void initState() {
    super.initState();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email;
      _foodStream = FirebaseFirestore.instance
          .collection('food_list')
          .where('email', isEqualTo: _email)
          // .orderBy('time', descending: true)
          .snapshots();
    });
  }

  Future<void> delFood(foodId) {
    return food_list
        .doc(foodId)
        .delete()
        .then((value) => print('Food delete'))
        .catchError((error) => print('Failed to delete food: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        //         Future<void> addWorkout() {
        //   return workouts
        //       .add({
        //         'time': Timestamp.now(),
        //         'nameWorkout': name_workout,
        //         'set': set,
        //         'totalCal': totalCal,
        //         'email': _email
        //       })
        //       .then((value) => print("add new workout"))
        //       .catchError((error) => print('Failed to add workout: $error'));
        // }
        print('$sum');
        Navigator.pop(
            context, MaterialPageRoute(builder: (context) => HomeWidget()));
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("ประวัติรายการอาหาร", style: GoogleFonts.lemon()),
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.lightBlue.shade800],
            )),
          ),
        ),
        body: Column(
          children: [
            // Text('Total: $sum'),
            Expanded(
              child: StreamBuilder(
                  stream: _foodStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something went wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Text('Loading...');
                    }
                    return Container(
                      margin: const EdgeInsets.all(15),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                              document.data()! as Map<String, dynamic>;
                          sum += data['totalCal'];
                          // print('${data['nameWorkout']} $sum');
                          return Container(
                            margin: const EdgeInsets.only(top: 2, bottom: 2),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(.2),
                                  blurRadius: 20.0,
                                ),
                              ],
                            ),
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              color: const Color(0xFFf4f4f4),
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              child: ListTile(
                                title: Text(data['nameFood']),
                                subtitle: Text(
                                    'จำนวน: ${data['qty']}  แคลอรี่: ${data['totalCal']}'),
                                trailing: IconButton(
                                    onPressed: () async {
                                      await delFood(document.id);
                                    },
                                    icon: const Icon(Icons.delete)),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FoodForm(
                                                foodId: document.id,
                                                nameFood: data['nameFood'],
                                                qty: data['qty'],
                                                totalCal: data['totalCal'],
                                                sum_food: data['totalCal'],
                                              )));
                                },
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
